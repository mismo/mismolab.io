import React, { Component } from 'react';
import './App.css';
import NavBar from './components/NavBar/NavBar';
import Info from './components/Info/Info';
import Title from  './components/Title/Title';
import { Waypoint } from 'react-waypoint';
import scrollToComponent from 'react-scroll-to-component';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state={page: 'About Me', prevPage: ''};
    this.changePage=this.changePage.bind(this);
    this.dynamicNav=this.dynamicNav.bind(this);
    this.staticNav=this.staticNav.bind(this);
  }

  componentDidMount(){
    this.setState({prevPage: document.getElementById('About Me')});
  }

  changePage(newPage, oldPage){
    this.setState({prevPage: oldPage})
    this.setState({page: newPage});
    scrollToComponent(this.topPage, { offset: 0, align: 'bottom', duration: 800, ease:'linear'});
  }

  dynamicNav({currentPosition, previousPosition}){
    if (currentPosition === Waypoint.above && previousPosition === Waypoint.inside && this.state.page!='About Me'){
      document.getElementById('sectionNavList').style.visibility='visible';
      document.getElementById('sectionNavList').style.opacity='1';
    }
  }

  staticNav({currentPosition, previousPosition}){
    if (currentPosition === Waypoint.inside && previousPosition === Waypoint.above && this.state.page!='About Me'){
      document.getElementById('sectionNavList').style.visibility='hidden';
      document.getElementById('sectionNavList').style.opacity='0';
    }
  }

  render() {
    return (
      <body >
        <div>
          <NavBar oldPage={this.state.prevPage} onChange={this.changePage}/>
        </div>
        <div className = 'banner'>
          <div className = 'bannerContent'>Welcome to a website all about Damien Chan!</div>
        </div>
        <Waypoint onLeave={this.dynamicNav} onEnter={this.staticNav}>
          <div id="portraitContainer">
            <img id="portrait" src={require('./images/portrait.jpg')}/>
          </div>
        </Waypoint>
        <div id="title">
          <Title title={this.state.page} ref={(section) => {this.topPage = section;}}/>
        </div>
        <div id="information">
          <Info page={this.state.page}/>
        </div>
      </body>
    );
  }
}

export default App;
