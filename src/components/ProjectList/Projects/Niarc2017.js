import React from 'react';
import './Niarc2017.css';

const niarc2017=
    {text: (
        <div className="projectContainer">
            <div>
                <h1>Niarc 2017</h1>
                <img className='logo' src={require('../../../images/images.png')} />
            </div>
            <div>
                <p>
                    Niarc (National Instruments Autonomous Robotics Competition) is an annual competition where teams from 
                    different universities are to design and program an autonomous system to navigate a given course. This
                    course changes every year and so being able to overcome the course's obstacles through the use of effective 
                    algorithms and innovative yet simple design is the key to success.
                </p>
                <p>
                    As a newcomer to this competition this year, I assisted the mechanical team in manufacturing the chassis as 
                    well as organising the construction of a mock field. Although I had a fair amount of mechanical experience,
                    I was able to take on fairly simple design tasks using AutoDesk Invtor CAD software. Some of these tasks
                    included designing parts for the field as well as a tool cabinet for transporting and storing tools and
                    equipment for the day of the competition. These tasks assisted in getting me started in learning the basics
                    of 3D computer design software.
                </p>
            </div>
            <div>
                <img className='niarc2017' src={require('../../../images/pit_station.png')} />
                <img className='niarc2017' src={require('../../../images/19875870_728956943954298_62512860_o.jpg')} />
            </div>
        </div>
    ),
    index: 'Niarc 2017'
};

export default niarc2017;