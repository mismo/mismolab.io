import React from 'react';
import './FirstWebapp.css';

const firstWebapp=
    {text: (
        <div className="projectContainer">
            <div>
                <h1>Personal Blog Webapp</h1>
            </div>
            <div>
                <p>
                   Yes, I am referring to this website itself. 
                </p>
                <p>
                    I used the popular component-based framework, React, to create this amazing personal website.
                    Although I had some knowledge of web development through a course at university, I had to brush
                    up on basic HTML and git version control as well as learn CSS styling, JavaScript and of course,
                    React from scratch. I did this through an online coding course hosted on Code Academy and within
                    a month, I was able to build a complete front-end webapp up and running.
                </p>
                <p>
                    Some core concepts of the React framework that I used to make this webapp include, JSX, components 
                    and their lifecycles, passing props to component children, changing states using props, events 
                    and event handlers, modifying components using the DOM and accessing instance of components using 
                    refs. This was my first attempt at using React and I hope to gain more experience using React in 
                    the near future to make even better webapps.
                </p>
            </div>
            <div>
                <img className='logos' src={require('../../../images/index.png')} />
                <img className='logos' src={require('../../../images/1*eXIBeNlLhz4Pe6vDrYkXLQ.png')} />
                <img className='logos' src={require('../../../images/javascript_logo.png')} />
                <img className='logos' src={require('../../../images/1*3SVfBkNZI2f-sspiq59xcw.png')} />
                <img className='logos' src={require('../../../images/1*l9IZ_LeUCP_vwxjDKI2Xgw.jpeg')} />
            </div>
        </div>
    ),
    index: 'Personal Blog'
};

export default firstWebapp;