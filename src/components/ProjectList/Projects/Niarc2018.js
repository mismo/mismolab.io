import React from 'react';
import './Niarc2018.css';

const niarc2018=
    {text: (
        <div className="projectContainer">
            <div>
                <h1>Niarc 2018</h1>
                <img className='logo' src={require('../../../images/images.png')} />
            </div>
            <div>
                <p>
                    Niarc (National Instruments Autonomous Robotics Competition) is an annual competition where teams from 
                    different universities are to design and program an autonomous system to navigate a given course. This
                    course changes every year and so being able to overcome the course's obstacles through the use of effective 
                    algorithms and innovative yet simple design is the key to success.
                </p>
                <p>
                    As the leader of the mechanical team this year, I was responsible for the design and manufacture of the
                    system's chassis as well as extra mounts for sensors and other equipment. The course contained
                    obstacles that needed to be avoided, resulting in the design requirement of the chassis to be as small as
                    possible to reduce the robot's footprint. This proved to be challenging due to the sizeable amount of 
                    sensors and equipment that the chassis had to hold. Despite this, after a few iterations, I was able to
                    come up with an effective design which allowed me to develop my CAD skills using AutoDesk Inventor.
                </p>
                <div>
                    < img src={require('../../../images/36294315_1488998001204309_264402567608401920_n.jpg')} id='earlyRev1'/>
                    <img src={require('../../../images/36330925_1488998114537631_5102341879731585024_n.jpg')} id='earlyRev2'/>
                </div>
                <p>
                    In addition to design, I took part in the manufacture of the chassis. The designs were mainly manufactured 
                    using laser cutter provided by UNSW's makerspaces and my own personal 3D printer. I also assisted in wiring
                    the system's internal components, allowing me to gain a better understanding in motor controllers, 
                    micro-controllers and sensors such as lidars.
                </p>
            </div>
            <div>
                    
            </div>
        </div>
    ),
    index: 'Niarc 2018'
};

export default niarc2018;