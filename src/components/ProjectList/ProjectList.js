import React from 'react';
import TextBox from '../TextBox/TextBox';
import SectionNav from  '../SectionNav/SectionNav';
import niarc2018 from './Projects/Niarc2018';
import niarc2017 from './Projects/Niarc2017';
import firstWebapp from './Projects/FirstWebapp';
import './ProjectList.css';

const list = [niarc2018, niarc2017, firstWebapp];

class ProjList extends React.Component{
    refList = {};
    
/*
    componentDidMount(){
        const textBox = -165 - (list.length)*39;
        document.getElementById('projList').style.marginTop=`${textBox}px`;
        const navButton = 180 + (list.length)*39;
        document.getElementById('sectionNavList').style.top=`${navButton}px`;
    }
*/

    render(){
        return (
            <div id="projList">
                <div className="sectionNav">
                    <SectionNav list={list} refList={this.refList}/>
                </div>
                {list.map(proj=>{
                    return (
                        <div>
                            <TextBox body={proj.text} id={proj.index} ref={(section) => {this.refList[proj.index] = section;}}/>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default ProjList;
