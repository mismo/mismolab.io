import React from 'react';
import TextBox from '../TextBox/TextBox';
import SectionNav from  '../SectionNav/SectionNav';
import BlogFeb19 from './Blogs/Feb19';
import BlogMar19 from './Blogs/Mar19';
import './BlogList.css';

const list = [BlogFeb19];

class BlogList extends React.Component{
    refList = {};

/*
    componentDidMount(){
        const textBox = -165 - (list.length)*39;
        document.getElementById('blogList').style.marginTop=`${textBox}px`;
        const navButton = 180 + (list.length)*39;
        document.getElementById('sectionNavList').style.top=`${navButton}px`;
        }
    }
*/  

    render(){
        return (
            <div id="blogList">
                <div className="sectionNav">
                    <SectionNav list={list} refList={this.refList} />
                </div>
                {list.map(blog=>{
                    return (
                        <div>
                            <TextBox body={blog.text} id={blog.index} ref={(section) => {this.refList[blog.index] = section;}}/>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default BlogList;