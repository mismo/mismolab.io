import React from 'react';
import './Feb19.css';

const Feb19 = {
    text: (
        <div className="blogContainer">
            <div className="date">Date: 28/2/19</div>
            <div>
                <p>It’s finally done!</p>
                <p>
                    My very first website made using React is up and running. After taking a university course 
                    that required the development of a website, I acquired a new-found interest in web development 
                    and all its wonders. I realised that it isn’t really just about programming a website, but in 
                    fact, it really is a whole culture that ranges from red bull fuelled sprints, to brainstorms 
                    over lunch.
                </p>
                <p>
                    With this newly hatched interest in my mind, I decided to nurture the idea by taking an 
                    online course on development using a new framework called React. That’s basically how my month 
                    started and has continued since then. I spent 2-3 weeks learning the basics, HTML, CSS, JavaScript 
                    and finally, React itself. This course was thorough and provided many projects and quizzes that 
                    really helped set the core concepts of React in my brain.
                </p>
                <div>
                    <img id='reactLogo' src={require('../../../images/1*i3hzpSEiEEMTuWIYviYweQ.png')} />
                </div>
                <p>
                    I then took all that I had l learnt and made this website! Although it may not seem like much, I 
                    put all of my effort and knowledge into it and I’m honestly quite proud of how it turned out. I was 
                    able to utilise components to structure the website and I also used props to pass data between 
                    components in order to create features such as the textboxes in each section. These are just some of 
                    the many things that React provides to a web developer and I am extremely keen to learn more in the 
                    near future.
                </p>
                <p>I will continue to post every month about the things I learnt; not just about web development, but my 
                    personal and university projects and even things I learnt in courses.
                </p>
            </div>
        </div>
    ),
    index: 'February 2019'
};

export default Feb19;