import React from 'react';
import './Info.css'
import About from '../About/About'
import BlogList from '../BlogList/BlogList';
import ProjectList from '../ProjectList/ProjectList';

class Info extends React.Component{
    constructor(props){
        super(props);
        this.chooseRender=this.chooseRender.bind(this);
    }

    chooseRender(){
        if (this.props.page === 'Projects')
            return <ProjectList />;
        else if (this.props.page === 'Blog') 
            return <BlogList />;
        else if (this.props.page === 'About Me')
            return <About />;
      }

    render(){
        return <div id="infoBox">{this.chooseRender()}</div>;
    }
}

export default Info;