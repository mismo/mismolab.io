import React from 'react';
import './Title.css';

class Title extends React.Component{
    constructor(props){
        super(props);
    }

    render (){
        return (
            <div className='title'>
                <div className='titleBanner'>
                    <div>{this.props.title}</div>
                </div>
            </div>
        )
    }
}

export default Title;
