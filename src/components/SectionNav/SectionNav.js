import React from 'react';
import './SectionNav.css';
import scrollToComponent from 'react-scroll-to-component';

class SectionNav extends React.Component{
    render(){
        return (
            <div id="sectionNavList">
                {this.props.list.map(item=>{
                    return (
                        <div>
                            <button id="sectionButton" type="button" onClick={() => scrollToComponent(this.props.refList[item.index], { offset: 0, align: 'top', duration: 700})}>{item.index}</button>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default SectionNav;
