import React from 'react';

const AboutMe=
    {text: (
        <div className='aboutContainer'>
            <div>
                <p>
                    My name is Damien Chan and I am a university student studying Mechatronics and Computer Science. 
                    I have a strong interest in technology and programming. Because of this, I have taken part in various
                    robotics competitions starting from when I was in high school.
                </p>
                <p>
                    Although my initial interest was in 
                    mechanical design, I recently gained a new-found passion for the programming aspect of things. This has 
                    in turn led to the exploration of certain fields such as web development. With this website, I hope to 
                    be able to track my personal growth through my achievements and findings.
                </p>
                <p>
                    Without further ado, lets
                    get straight into it!
                </p>
            </div>
            <img id='cat' src={require('../../images/lazycatkeyboard-448x210.jpg')} />
        </div>
    )
};

export default AboutMe;
