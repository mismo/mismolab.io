import React from 'react';
import TextBox from '../TextBox/TextBox';
import AboutMe from './AboutMe'
import './About.css';

class About extends React.Component{
    render(){
        return (
            <div id="about">
                <div>
                    <TextBox body={AboutMe.text}/>
                </div>
            </div>
        )
    }
}

export default About;
