import React from 'react';
import './NavBar.css';

class NavBar extends React.Component{
    constructor(props){
        super(props);
        this.handlePageChange=this.handlePageChange.bind(this);
    }

    handlePageChange(e){
        this.props.onChange(e.target.id, e.currentTarget);

        //change color of previous page back to original
        this.props.oldPage.style.color = '#5ff';
        this.props.oldPage.style.fontStyle = 'unset';
        this.props.oldPage.style.fontWeight = 500;

        //highlight selected page tab in navbar
        e.currentTarget.style.color= '#66f';
        e.currentTarget.style.fontStyle = 'oblique';
        e.currentTarget.style.fontWeight= 900;
    }
    
    render(){
        return (
            <div className='navBackground'>
                <ul className="navBar">
                    <li onClick={this.handlePageChange} id='About Me'>About Me</li>
                    <li onClick={this.handlePageChange} id='Projects'>Projects</li>
                    <li onClick={this.handlePageChange} id='Blog'>Blog</li>
                </ul>
            </div>
        ) 
    }
}
 
export default NavBar;