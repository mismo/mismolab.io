import React from 'react';
import './TextBox.css'

class TextBox extends React.Component {
    constructor(props){
        super(props);
    }

    addShadow(event){
        event.target.style.boxShadow = "3px 6px #888888";
        event.target.style.borderLeft = "3px";
        event.target.style.borderTop = "6px";
        event.target.style.paddingBottom="0px";
        event.target.style.marginBottom="46px";
    }

    removeShadow(event){
        event.target.style.boxShadow = "0px 0px";
        event.target.style.border= "1px solid rgb(169,169,169)";
        event.target.style.paddingBottom="6px";
        event.target.style.marginBottom="40px";
    }

    render(){
        return (
            <div className="textBox" onMouseOver={this.addShadow} onMouseLeave={this.removeShadow}>
                <div className='text'> 
                    {this.props.body}
                </div>
            </div>
        )
    }
}

export default TextBox;